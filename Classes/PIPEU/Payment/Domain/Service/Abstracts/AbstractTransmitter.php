<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Service\Abstracts;

use PIPEU\Geo\Domain\Model\Interfaces\InterfaceCountry;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentDataInterface;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentTypeInterface;
use PIPEU\Payment\Domain\Model\Interfaces\TransactionInterface;
use PIPEU\Payment\Domain\Service\Exceptions\CountryNotSupportedException;
use PIPEU\Payment\Domain\Service\Exceptions\InvalidTransmitterPriorityException;
use PIPEU\Payment\Domain\Service\Exceptions\MaximumAmountException;
use PIPEU\Payment\Domain\Service\Exceptions\MinimumAmountException;
use PIPEU\Payment\Domain\Service\Exceptions\PaymentTypeNotSupportedException;
use PIPEU\Payment\Domain\Service\Exceptions\TransactionFailedException;
use PIPEU\Payment\Domain\Service\Interfaces\TransmitterInterface;
use PIPEU\Payment\Settings\Settings;
use PIPEU\Payment\Settings\Interfaces\SettingsInterface;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Flow\Http\Client\Browser;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Http\Request;
use TYPO3\Flow\Http\Response;

/**
 * Class AbstractTransmitter
 *
 * @package PIPEU\Payment\Domain\Service\Abstracts
 * @Flow\Scope("singleton")
 */
abstract class AbstractTransmitter implements TransmitterInterface {

	const PRIORITY = 0;

	/**
	 * @var SettingsInterface
	 */
	protected $settings;

	/**
	 * @var Browser
	 * @Flow\Inject
	 */
	protected $browser;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * @param array $settings
	 * @return void
	 */
	public function injectSettings(array $settings = array()) {
		$this->settings = new Settings($settings);
	}

	/**
	 * @return string
	 */
	public static function getType() {
		$static = new static();
		return get_class($static);
	}

	/**
	 * @return integer
	 * @throws InvalidTransmitterPriorityException
	 */
	public static function getPriority() {
		if (static::PRIORITY === NULL) {
			throw new InvalidTransmitterPriorityException('Transmitter does not have a valid priority definition. Type defitition is of type of "NULL" but "integer" is expected.', 1414314675);
		}
		if (!is_integer(static::PRIORITY)) {
			throw new InvalidTransmitterPriorityException('Transmitter does not have a valid priority definition. Type defitition is of type of "' . gettype(static::PRIORITY) . '" but "integer" is expected.', 1414314676);
		}
		return (integer)static::PRIORITY;
	}

	/**
	 * @return SettingsInterface
	 */
	public function getSettings() {
		return $this->settings;
	}

	/**
	 * @param TransactionInterface $transaction
	 * @param string $uri
	 * @param string $method
	 * @return $this
	 * @throws TransactionFailedException
	 */
	public function send(TransactionInterface $transaction, $uri, $method = 'GET') {
		try {
			$this->response = $this->browser->request($uri, $method, $transaction->getArrayCopy());
			$this->request = $this->browser->getLastRequest();
			return $this;
		} catch (\Exception $exception) {
			throw new TransactionFailedException('Transaction failed', 1414499839, $exception);
		}
	}

	/**
	 * @return Request
	 */
	public function getRequest() {
		return $this->request;
	}

	/**
	 * @return Response
	 */
	public function getResponse() {
		return $this->response;
	}

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @param PaymentDataInterface $paymentData
	 * @return boolean
	 * @throws CountryNotSupportedException
	 * @throws MinimumAmountException
	 * @throws MaximumAmountException
	 * @throws PaymentTypeNotSupportedException
	 */
	public function canHandle(PaymentTypeInterface $paymentType, PaymentDataInterface $paymentData) {

		if ($this->isActivePaymentType($paymentType) === FALSE) {
			throw new PaymentTypeNotSupportedException('Type of payment not supported', 1414330306);
		}

		$canHandleCountry = FALSE;
		$canHandleMinimumAmount = FALSE;
		$canHandleMaximumAmount = FALSE;

		$secondaryCountry = ObjectAccess::getPropertyPath($paymentData, 'bookingSecondaryPostal.country');
		$primaryCountry = ObjectAccess::getPropertyPath($paymentData, 'bookingSecondaryPostal.country');

		if ($canHandleCountry === FALSE && $secondaryCountry instanceof InterfaceCountry) {
			$canHandleCountry = $this->canHandleCountry($paymentType, $secondaryCountry);
		}

		if ($canHandleCountry === FALSE && $primaryCountry instanceof InterfaceCountry) {
			$canHandleCountry = $this->canHandleCountry($paymentType, $primaryCountry);
		}

		if ($canHandleCountry === FALSE) {
			throw new CountryNotSupportedException('Country not supported', 1414326520);
		}

		if ($canHandleMinimumAmount === FALSE) {
			$canHandleMinimumAmount = $this->canHandleMinimumAmount($paymentType, $paymentData);
		}

		if ($canHandleMinimumAmount === FALSE) {
			throw new MinimumAmountException('Minimum amount not reached', 1414326521);
		}

		if ($canHandleMaximumAmount === FALSE) {
			$canHandleMaximumAmount = $this->canHandleMaximumAmount($paymentType, $paymentData);
		}

		if ($canHandleMaximumAmount === FALSE) {
			throw new MaximumAmountException('Maximum amount reached', 1414326522);
		}

		return TRUE;
	}

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @return boolean
	 */
	public function isActivePaymentType(PaymentTypeInterface $paymentType) {
		return (boolean)$this->getSettings()->getValueByPath('PaymentTypes.' . $paymentType->getType() . '.isEnabled');
	}

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @param InterfaceCountry $country
	 * @return boolean
	 */
	public function canHandleCountry(PaymentTypeInterface $paymentType, InterfaceCountry $country) {
		$countryLimitation = $this->getSettings()->getValueByPath('PaymentTypes.' . $paymentType->getType() . '.countryLimitation');
		if ($countryLimitation === NULL) {
			return TRUE;
		} else {
			$countryLimitation = (array)$countryLimitation;
			if (in_array(strtoupper($country->getIso2()), $countryLimitation)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @param PaymentDataInterface $paymentData
	 * @return boolean
	 */
	public function canHandleMaximumAmount(PaymentTypeInterface $paymentType, PaymentDataInterface $paymentData) {
		$limit = $this->getSettings()->getValueByPath('PaymentTypes.' . $paymentType->getType() . '.maximumAmount');
		if ($limit === NULL) {
			return TRUE;
		} else {
			$limit = (float)$limit;
			if ($paymentData->getBookingAmount() <= $limit) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @param PaymentDataInterface $paymentData
	 * @return boolean
	 */
	public function canHandleMinimumAmount(PaymentTypeInterface $paymentType, PaymentDataInterface $paymentData) {
		$limit = $this->getSettings()->getValueByPath('PaymentTypes.' . $paymentType->getType().'.minimumAmount');
		if ($limit === NULL) {
			return TRUE;
		} else {
			$limit = (float)$limit;
			if ($paymentData->getBookingAmount() >= $limit) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}
