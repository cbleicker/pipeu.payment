<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Service\Interfaces;

use PIPEU\Geo\Domain\Model\Interfaces\InterfaceCountry;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentTypeInterface;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentDataInterface;
use PIPEU\Payment\Domain\Model\Interfaces\TransactionInterface;
use TYPO3\Flow\Http\Request;
use TYPO3\Flow\Http\Response;
use PIPEU\Payment\Domain\Service\Exceptions\TransmitterHandleException;
use PIPEU\Payment\Settings\Interfaces\SettingsInterface;

/**
 * Class TransmitterInterface
 *
 * @package PIPEU\Payment\Domain\Service\Interfaces
 */
interface TransmitterInterface {

	/**
	 * @return string
	 */
	public static function getType();

	/**
	 * @return integer
	 */
	public static function getPriority();

	/**
	 * @param TransactionInterface $transaction
	 * @param string $uri
	 * @param string $method
	 * @return $this
	 */
	public function send(TransactionInterface $transaction, $uri, $method = 'GET');

	/**
	 * @return Request
	 */
	public function getRequest();

	/**
	 * @return Response
	 */
	public function getResponse();

	/**
	 * @return SettingsInterface
	 */
	public function getSettings();

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @param PaymentDataInterface $paymentData
	 * @return boolean
	 * @throws TransmitterHandleException
	 */
	public function canHandle(PaymentTypeInterface $paymentType, PaymentDataInterface $paymentData);

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @return boolean
	 */
	public function isActivePaymentType(PaymentTypeInterface $paymentType);

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @param InterfaceCountry $country
	 * @return boolean
	 */
	public function canHandleCountry(PaymentTypeInterface $paymentType, InterfaceCountry $country);

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @param PaymentDataInterface $paymentData
	 * @return boolean
	 */
	public function canHandleMaximumAmount(PaymentTypeInterface $paymentType, PaymentDataInterface $paymentData);

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @param PaymentDataInterface $paymentData
	 * @return boolean
	 */
	public function canHandleMinimumAmount(PaymentTypeInterface $paymentType, PaymentDataInterface $paymentData);
}
