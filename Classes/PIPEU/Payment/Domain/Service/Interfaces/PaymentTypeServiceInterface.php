<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Service\Interfaces;

use Doctrine\Common\Collections\Collection;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentDataInterface;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentTypeInterface;

/**
 * Class PaymentTypeServiceInterface
 *
 * @package PIPEU\Payment\Domain\Service\Interfaces
 */
interface PaymentTypeServiceInterface {

	/**
	 * @param PaymentDataInterface $paymentData
	 * @return Collection<PaymentTypeInterface>
	 */
	public function getSupportedPaymentTypes(PaymentDataInterface $paymentData);

	/**
	 * @return Collection<PaymentTypeInterface>
	 */
	public function getActivePaymentTypes();
}
