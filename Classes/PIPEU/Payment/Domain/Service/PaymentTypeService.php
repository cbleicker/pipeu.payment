<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentDataInterface;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentTypeInterface;
use PIPEU\Payment\Domain\Service\Exceptions\TransmitterHandleException;
use PIPEU\Payment\Domain\Service\Interfaces\PaymentTypeServiceInterface;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Property\PropertyMapper;
use TYPO3\Flow\Property\PropertyMappingConfigurationBuilder;
use TYPO3\Flow\Property\TypeConverter\ObjectConverter;
use PIPEU\Payment\Domain\Service\Interfaces\TransmitterInterface as PaymentTransmitterInterface;

/**
 * Class PaymentTypeService
 *
 * @package PIPEU\Payment\Domain\Service
 */
class PaymentTypeService implements PaymentTypeServiceInterface {

	const PAYMENT_TYPE_INTERFACE = 'PIPEU\Payment\Domain\Model\Interfaces\PaymentTypeInterface';

	/**
	 * @var PaymentTransmitterInterface
	 * @Flow\Inject
	 */
	protected $paymentTransmitter;

	/**
	 * @var ReflectionService
	 * @Flow\Inject
	 */
	protected $reflectionService;

	/**
	 * @var PropertyMapper
	 * @Flow\Inject
	 */
	protected $propertyMapper;

	/**
	 * @var PropertyMappingConfigurationBuilder
	 * @Flow\Inject
	 */
	protected $propertyMappingConfigurationBuilder;

	/**
	 * @param PaymentDataInterface $paymentData
	 * @return Collection<PaymentTypeInterface>
	 */
	public function getSupportedPaymentTypes(PaymentDataInterface $paymentData) {
		return $this->getPaymentTypes()->filter($this->paymentTransmitterPaymentTypeFilter($paymentData));
	}

	/**
	 * @return Collection<PaymentTypeInterface>
	 */
	public function getActivePaymentTypes() {
		return $this->getPaymentTypes()->filter($this->paymentTransmitterActivePaymentTypeFilter());
	}

	/**
	 * @return Collection<PaymentTypeInterface>
	 */
	protected function getPaymentTypes() {
		$propertyMappingConfiguration = $this->propertyMappingConfigurationBuilder->build();
		$availablePaymentTypeImplementations = $this->reflectionService->getAllImplementationClassNamesForInterface(static::PAYMENT_TYPE_INTERFACE);
		$source = new \ArrayObject();
		/** @var $paymentTypeImplementation */
		foreach ($availablePaymentTypeImplementations as $paymentTypeNum => $paymentTypeImplementation) {
			$source->append($this->getPaymentTypeSource($paymentTypeImplementation));
			$propertyMappingConfiguration->forProperty($paymentTypeNum)->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\ObjectConverter', ObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE);
		}
		$paymentTypes = $this->propertyMapper->convert($source->getArrayCopy(), 'Doctrine\Common\Collections\Collection<' . self::PAYMENT_TYPE_INTERFACE . '>', $propertyMappingConfiguration);
		/** @var \ArrayIterator $iterator */
		$iterator = $paymentTypes->getIterator();
		$iterator->uasort($this->prioritySorting());
		$sortedByPriority = new ArrayCollection($iterator->getArrayCopy());
		return $sortedByPriority;
	}

	/**
	 * @param PaymentDataInterface $paymentData
	 * @return \Closure
	 */
	protected function paymentTransmitterPaymentTypeFilter(PaymentDataInterface $paymentData) {
		return function (PaymentTypeInterface $paymentType) use ($paymentData) {
			try {
				return $this->paymentTransmitter->canHandle($paymentType, $paymentData);
			} catch (\Exception $exception) {
				return FALSE;
			}
		};
	}

	/**
	 * @return \Closure
	 */
	protected function paymentTransmitterActivePaymentTypeFilter() {
		return function (PaymentTypeInterface $paymentType) {
			return $this->paymentTransmitter->isActivePaymentType($paymentType);
		};
	}

	/**
	 * @return \Closure
	 */
	protected function prioritySorting() {
		return function (PaymentTypeInterface $a, PaymentTypeInterface $b) {
			$priorityA = (integer)$this->paymentTransmitter->getSettings()->getValueByPath('PaymentTypes.' . $a->getType() . '.priority');
			$priorityB = (integer)$this->paymentTransmitter->getSettings()->getValueByPath('PaymentTypes.' . $b->getType() . '.priority');
			if ($priorityA === $priorityB) {
				return 0;
			} else {
				return $priorityA > $priorityB ? -1 : 1;
			}
		};
	}

	/**
	 * @param string $type
	 * @return array
	 */
	protected function getPaymentTypeSource($type) {
		return array(
			'__type' => $type
		);
	}
}
