<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model;

use PIPEU\Payment\Domain\Model\Abstracts\AbstractTransfer;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class EpsTransfer
 *
 * @package PIPEU\Payment\Domain\Model
 */
class EpsTransfer extends AbstractTransfer {

	/**
	 * @var string
	 */
	protected $bankGroupType;

	/**
	 * @param string $bankGroupType
	 * @return $this
	 */
	public function setBankGroupType($bankGroupType = NULL) {
		$this->bankGroupType = $bankGroupType;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getBankGroupType() {
		return $this->bankGroupType;
	}
}
