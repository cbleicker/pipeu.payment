<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Abstracts;

use PIPEU\Payment\Domain\Model\Interfaces\PaymentDataInterface;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentTypeInterface;
use PIPEU\Payment\Domain\Model\Interfaces\TransactionInterface;
use PIPEU\Payment\Domain\Service\Exceptions\PaymentTypeNotSupportedException;
use PIPEU\Payment\Settings\Interfaces\SettingsInterface;
use PIPEU\Payment\Settings\Settings;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Flow\Utility\Arrays;

/**
 * Class AbstractTransaction
 *
 * @package PIPEU\Payment\Domain\Model\Abstracts
 */
abstract class AbstractTransaction implements TransactionInterface {

	/**
	 * @var \ArrayObject
	 */
	protected $storage;

	/**
	 * @var SettingsInterface
	 */
	protected $settings;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->storage = new \ArrayObject();
		$this->settings = new Settings();
	}

	/**
	 * @param array $settings
	 * @return $this
	 */
	public function injectSettings(array $settings = array()) {
		$this->settings->exchangeArray($settings);
		return $this;
	}

	/**
	 * @return SettingsInterface
	 */
	public function getSettings() {
		return $this->settings;
	}

	/**
	 * @return \ArrayObject
	 */
	public function getStorage() {
		return $this->storage;
	}

	/**
	 * @param mixed $index
	 * @return $this
	 */
	public function offsetExists($index) {
		return $this->getStorage()->offsetExists($index);
	}

	/**
	 * @param mixed $index
	 * @return mixed
	 */
	public function offsetGet($index) {
		return $this->getStorage()->offsetGet($index);
	}

	/**
	 * @param mixed $index
	 * @param mixed $newval
	 * @return $this
	 */
	public function offsetSet($index, $newval) {
		$this->getStorage()->offsetSet($index, $newval);
		return $this;
	}

	/**
	 * @param mixed $index
	 * @return $this
	 */
	public function offsetUnset($index) {
		$this->getStorage()->offsetUnset($index);
		return $this;
	}

	/**
	 * @param mixed $value
	 * @return $this
	 */
	public function append($value) {
		$this->getStorage()->append($value);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getArrayCopy() {
		return $this->getStorage()->getArrayCopy();
	}

	/**
	 * @return integer
	 */
	public function count() {
		return $this->getStorage()->count();
	}

	/**
	 * @return integer
	 */
	public function getFlags() {
		return $this->getStorage()->getFlags();
	}

	/**
	 * @param integer $flags
	 * @return $this
	 */
	public function setFlags($flags) {
		$this->getStorage()->setFlags($flags);
		return $this;
	}

	/**
	 * @return $this
	 */
	public function asort() {
		$this->getStorage()->asort();
		return $this;
	}

	/**
	 * @return $this
	 */
	public function ksort() {
		$this->getStorage()->ksort();
		return $this;
	}

	/**
	 * @param callable $cmp_function
	 * @return $this
	 */
	public function uasort($cmp_function) {
		$this->getStorage()->uasort($cmp_function);
		return $this;
	}

	/**
	 * @param callable $cmp_function
	 * @return $this
	 */
	public function uksort($cmp_function) {
		$this->getStorage()->uksort($cmp_function);
		return $this;
	}

	/**
	 * @return $this
	 */
	public function natsort() {
		$this->getStorage()->natsort();
		return $this;
	}

	/**
	 * @return $this
	 */
	public function natcasesort() {
		$this->getStorage()->natcasesort();
		return $this;
	}

	/**
	 * @param string $serialized
	 * @return $this
	 */
	public function unserialize($serialized) {
		$this->getStorage()->unserialize($serialized);
		return $this;
	}

	/**
	 * @return string
	 */
	public function serialize() {
		return $this->getStorage()->serialize();
	}

	/**
	 * @return \ArrayIterator
	 */
	public function getIterator() {
		return $this->getStorage()->getIterator();
	}

	/**
	 * @param array $input
	 * @return $this
	 */
	public function exchangeArray(array $input) {
		$this->getStorage()->exchangeArray($input);
		return $this;
	}

	/**
	 * @param string $iterator_class
	 * @return $this
	 */
	public function setIteratorClass($iterator_class) {
		$this->getStorage()->setIteratorClass($iterator_class);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getIteratorClass() {
		$this->getStorage()->getIteratorClass();
	}
}
