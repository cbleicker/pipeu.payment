<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Abstracts;

use PIPEU\Payment\Domain\Model\Interfaces\TransactionResultInterface;
use TYPO3\Flow\Error\Error;
use TYPO3\Flow\Error\Result;
use TYPO3\Flow\Http\Response as HttpResponse;
use TYPO3\Flow\Http\Response;
use TYPO3\Flow\Reflection\ObjectAccess;
use PIPEU\Payment\Settings\Settings;
use PIPEU\Payment\Settings\Interfaces\SettingsInterface;

/**
 * Class AbstractTransactionResult
 *
 * @package PIPEU\Payment\Domain\Model\Abstracts
 */
abstract class AbstractTransactionResult extends Result implements TransactionResultInterface {

	const GENERIC_ERROR_PROPERTY_NAME = 'genericTransactionErrors';

	/**
	 * @var Response
	 */
	protected $rawResponse;

	/**
	 * @var \ArrayObject
	 */
	protected $responseData;

	/**
	 * @param Response $response
	 */
	protected function __construct(Response $response) {
		$this->rawResponse = $response;
		$this->createResponseData();
		$this->setErrorsFromResponseData();
	}

	/**
	 * @return $this
	 */
	abstract protected function createResponseData();

	/**
	 * @return $this
	 */
	abstract protected function setErrorsFromResponseData();

	/**
	 * @return Response
	 */
	public function getRawResponse() {
		return $this->rawResponse;
	}

	/**
	 * @return \ArrayObject
	 */
	public function getResponseData() {
		return $this->responseData;
	}

	/**
	 * @return string
	 */
	public function getGenericErrorPropertyName() {
		return (string)static::GENERIC_ERROR_PROPERTY_NAME;
	}

	/**
	 * @param Response $response
	 * @return static
	 */
	public static function create(Response $response) {
		return new static($response);
	}
}
