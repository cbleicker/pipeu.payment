<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Abstracts;

use PIPEU\Payment\Domain\Model\Interfaces\CreditCardInterface;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentTypeInterface;
use PIPEU\Payment\Domain\Model\Interfaces\TransferInterface;
use PIPEU\Payment\Domain\Service\Exceptions\MultipleSuperTypesException;
use PIPEU\Payment\Domain\Service\Exceptions\UnknownSuperTypeException;
use PIPEU\Payment\Settings\Settings;
use PIPEU\Payment\Settings\Interfaces\SettingsInterface;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class AbstractPaymentType
 *
 * @package PIPEU\Payment\Domain\Model\Abstracts
 */
abstract class AbstractPaymentType implements PaymentTypeInterface {

	/**
	 * @var Settings
	 * @Flow\Transient
	 */
	protected $settings;

	/**
	 * @param array $settings
	 * @return void
	 */
	public function injectSettings(array $settings = array()) {
		$this->settings = new Settings($settings);
	}

	/**
	 * @return SettingsInterface
	 */
	public function getSettings() {
		return $this->settings;
	}

	/**
	 * @return SettingsInterface
	 */
	public function getTypeSettings() {
		$settings = (array)$this->getSettings()->getValueByPath('types.' . $this->getType());
		return new Settings($settings);
	}

	/**
	 * @return string
	 */
	public static function getType() {
		$static = new static();
		return get_class($static);
	}

	/**
	 * @return string
	 * @throws UnknownSuperTypeException
	 * @throws MultipleSuperTypesException
	 */
	public function getSuperType() {
		if($this instanceof CreditCardInterface && $this instanceof TransferInterface){
			throw new MultipleSuperTypesException('Multiple SuperTypes implemented. Either CreditCard- OR TransferInterface must be implemented for any kind of PaymentType.', 1414502685);
		}
		if ($this instanceof CreditCardInterface) {
			return 'PIPEU\Payment\Domain\Model\Interfaces\CreditCardInterface';
		} elseif ($this instanceof TransferInterface) {
			return 'PIPEU\Payment\Domain\Model\Interfaces\TransferInterface';
		} else{
			throw new UnknownSuperTypeException('Unkown Payment SuperType. Either CreditCard- or TransferInterface must be implemented for any kind of PaymentType.', 1414502686);
		}
	}
}
