<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Abstracts;

use PIPEU\Payment\Domain\Model\Interfaces\ExpirationDateInterface;

/**
 * Class AbstractExpirationDate
 *
 * @package PIPEU\Payment\Domain\Model\Abstracts
 */
abstract class AbstractExpirationDate implements ExpirationDateInterface {

	/**
	 * @var integer
	 */
	protected $month;

	/**
	 * @var integer
	 */
	protected $year;

	/**
	 * @param integer $month
	 * @return $this
	 */
	public function setMonth($month = NULL) {
		$this->month = $month;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getMonth() {
		return $this->month;
	}

	/**
	 * @param integer $year
	 * @return $this
	 */
	public function setYear($year = NULL) {
		$this->year = $year;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getYear() {
		return $this->year;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return (string)($this->getYear().$this->getMonth());
	}
}
