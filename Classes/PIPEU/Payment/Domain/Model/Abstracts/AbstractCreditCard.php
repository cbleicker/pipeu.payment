<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Abstracts;

use PIPEU\Payment\Domain\Model\Interfaces\CreditCardInterface;
use PIPEU\Payment\Domain\Model\ExpirationDate;

/**
 * Class AbstractCreditCard
 *
 * @package PIPEU\Payment\Domain\Model\Abstracts
 */
abstract class AbstractCreditCard extends AbstractPaymentType implements CreditCardInterface {

	/**
	 * @var string
	 */
	protected $holder;

	/**
	 * @var integer
	 */
	protected $number;

	/**
	 * @var ExpirationDate
	 */
	protected $expirationDate;

	/**
	 * @var integer
	 */
	protected $cvv;

	/**
	 * @param integer $cvv
	 * @return $this
	 */
	public function setCvv($cvv = NULL) {
		$this->cvv = $cvv;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getCvv() {
		return $this->cvv;
	}

	/**
	 * @param ExpirationDate $expirationDate
	 * @return $this
	 */
	public function setExpirationDate(ExpirationDate $expirationDate = NULL) {
		$this->expirationDate = $expirationDate;
		return $this;
	}

	/**
	 * @return ExpirationDate
	 */
	public function getExpirationDate() {
		return $this->expirationDate;
	}

	/**
	 * @param string $holder
	 * @return $this
	 */
	public function setHolder($holder = NULL) {
		$this->holder = $holder;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getHolder() {
		return $this->holder;
	}

	/**
	 * @param integer $number
	 * @return $this
	 */
	public function setNumber($number = NULL) {
		$this->number = $number;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getNumber() {
		return $this->number;
	}
}
