<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Abstracts;

use PIPEU\Payment\Domain\Model\Interfaces\TransferInterface;

/**
 * Class AbstractTransfer
 *
 * @package PIPEU\Payment\Domain\Model\Abstracts
 */
abstract class AbstractTransfer extends AbstractPaymentType implements TransferInterface {

	/**
	 * @var string
	 */
	protected $holder;

	/**
	 * @var string
	 */
	protected $number;

	/**
	 * @var string
	 */
	protected $bic;

	/**
	 * @param string $bic
	 * @return $this
	 */
	public function setBic($bic = NULL) {
		$this->bic = $bic;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getBic() {
		return $this->bic;
	}

	/**
	 * @param string $holder
	 * @return $this
	 */
	public function setHolder($holder = NULL) {
		$this->holder = $holder;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getHolder() {
		return $this->holder;
	}

	/**
	 * @param string $number
	 * @return $this
	 */
	public function setNumber($number = NULL) {
		$this->number = $number;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNumber() {
		return $this->number;
	}
}
