<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Abstracts;

use PIPEU\Payment\Domain\Model\Interfaces\LogInterface;
use TYPO3\Flow\Annotations as Flow;
use PIPEU\Factura\Domain\Abstracts\AbstractDocument;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractLog
 *
 * @package PIPEU\Payment\Domain\Model\Abstracts
 * @Flow\Entity
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractLog implements LogInterface {

	/**
	 * @var \DateTime
	 */
	protected $dateTime;

	/**
	 * @var string
	 * @ORM\Column(nullable=true)
	 */
	protected $transaction;

	/**
	 * @var AbstractDocument
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $document;

	/**
	 * @var array
	 * @ORM\Column(nullable=true)
	 */
	protected $data;

	/**
	 * @param array $data
	 * @param AbstractDocument $document
	 * @param string $transaction
	 */
	public function __construct(array $data = NULL, AbstractDocument $document = NULL, $transaction = NULL) {
		$this->dateTime = new \DateTime();
		$this->data = $data;
		$this->document = $document;
		$this->transaction = $transaction;
	}

	/**
	 * @param array $data
	 * @return $this
	 */
	public function setData(array $data = NULL) {
		$this->data = $data;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * @param AbstractDocument $document
	 * @return $this
	 */
	public function setDocument(AbstractDocument $document = NULL) {
		$this->document = $document;
		return $this;
	}

	/**
	 * @return AbstractDocument
	 */
	public function getDocument() {
		return $this->document;
	}

	/**
	 * @param string $transaction
	 * @return $this
	 */
	public function setTransaction($transaction = NULL) {
		$this->transaction = $transaction;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTransaction() {
		return $this->transaction;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateTime() {
		return $this->dateTime;
	}
}
