<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Interfaces;

use PIPEU\Factura\Domain\Abstracts\AbstractDocument;

/**
 * Interface LogInterface
 *
 * @package PIPEU\Payment\Domain\Model\Interfaces
 */
interface LogInterface {

	/**
	 * @return array
	 */
	public function getData();

	/**
	 * @return AbstractDocument
	 */
	public function getDocument();

	/**
	 * @return string
	 */
	public function getTransaction();

	/**
	 * @return \DateTime
	 */
	public function getDateTime();
}