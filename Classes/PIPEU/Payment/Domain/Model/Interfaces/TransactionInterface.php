<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Interfaces;

use PIPEU\Payment\Settings\Interfaces\SettingsInterface;

/**
 * Class TransactionInterface
 *
 * @package PIPEU\Payment\Domain\Model\Interfaces
 */
interface TransactionInterface extends \IteratorAggregate, \ArrayAccess, \Serializable, \Countable {

	/**
	 * @return \ArrayObject
	 */
	public function getStorage();

	/**
	 * @return SettingsInterface
	 */
	public function getSettings();

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @return static
	 */
	public static function create(PaymentTypeInterface $paymentType);

	/**
	 * @param PaymentTypeInterface $paymentType
	 * @return string
	 */
	public function getSimplePaymentType(PaymentTypeInterface $paymentType);

	/**
	 * @param TransactionInterface $transaction
	 * @return string
	 */
	public static function getHash(TransactionInterface $transaction);

	/**
	 * @param mixed $index
	 * @return boolean
	 */
	public function offsetExists($index);

	/**
	 * @param mixed $index
	 * @return mixed
	 */
	public function offsetGet($index);

	/**
	 * @param mixed $index
	 * @param mixed $newval
	 * @return $this
	 */
	public function offsetSet($index, $newval);

	/**
	 * @param mixed $index
	 * @return $this
	 */
	public function offsetUnset($index);

	/**
	 * @param mixed $value
	 * @return $this
	 */
	public function append($value);

	/**
	 * @return array
	 */
	public function getArrayCopy();

	/**
	 * @return integer
	 */
	public function count();

	/**
	 * @return integer
	 */
	public function getFlags();

	/**
	 * @param integer $flags
	 * @return $this
	 */
	public function setFlags($flags);

	/**
	 * @return $this
	 */
	public function asort();

	/**
	 * @return $this
	 */
	public function ksort();

	/**
	 * @param callback $cmp_function
	 * @return $this
	 */
	public function uasort($cmp_function);

	/**
	 * @param callback $cmp_function
	 * @return $this
	 */
	public function uksort($cmp_function);

	/**
	 * @return $this
	 */
	public function natsort();

	/**
	 * @return $this
	 */
	public function natcasesort();

	/**
	 * @param string $serialized
	 * @return $this
	 */
	public function unserialize($serialized);

	/**
	 * @return string
	 */
	public function serialize();

	/**
	 * @return \ArrayIterator
	 */
	public function getIterator();

	/**
	 * @param array $input
	 * @return $this
	 */
	public function exchangeArray(array $input);

	/**
	 * @param string $iterator_class
	 * @return $this
	 */
	public function setIteratorClass($iterator_class);

	/**
	 * @return string
	 */
	public function getIteratorClass();
}
