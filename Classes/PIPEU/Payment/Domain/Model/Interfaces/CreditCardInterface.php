<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Interfaces;
use PIPEU\Payment\Domain\Model\ExpirationDate;

/**
 * Class CreditCardInterface
 *
 * @package PIPEU\Payment\Domain\Model\Interfaces
 */
interface CreditCardInterface extends PaymentTypeInterface{

	/**
	 * @param integer $cvv
	 * @return $this
	 */
	public function setCvv($cvv = NULL);

	/**
	 * @return integer
	 */
	public function getCvv();

	/**
	 * @param ExpirationDate $expirationDate
	 * @return $this
	 */
	public function setExpirationDate(ExpirationDate $expirationDate = NULL);

	/**
	 * @return ExpirationDate
	 */
	public function getExpirationDate();

	/**
	 * @param string $holder
	 * @return $this
	 */
	public function setHolder($holder = NULL);

	/**
	 * @return string
	 */
	public function getHolder();

	/**
	 * @param integer $number
	 * @return $this
	 */
	public function setNumber($number = NULL);

	/**
	 * @return integer
	 */
	public function getNumber();

}
