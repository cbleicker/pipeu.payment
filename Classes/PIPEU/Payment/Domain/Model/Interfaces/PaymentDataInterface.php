<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Domain\Model\Interfaces;
use PIPEU\Geo\Domain\Model\Interfaces\InterfacePostal;
use TYPO3\Party\Domain\Model\PersonName;
use TYPO3\Party\Domain\Model\ElectronicAddress;

/**
 * Class PaymentDataInterface
 *
 * @package PIPEU\Payment\Domain\Model\Interfaces
 */
interface PaymentDataInterface {

	/**
	 * @return string
	 */
	public function getBookingPrimaryCompany();

	/**
	 * @return string
	 */
	public function getBookingSecondaryCompany();

	/**
	 * @return float
	 */
	public function getBookingAmount();

	/**
	 * @return InterfacePostal
	 */
	public function getBookingPrimaryPostal();

	/**
	 * @return InterfacePostal
	 */
	public function getBookingSecondaryPostal();

	/**
	 * @return PersonName
	 */
	public function getBookingPrimaryPersonName();

	/**
	 * @return PersonName
	 */
	public function getBookingSecondaryPersonName();

	/**
	 * @return ElectronicAddress
	 */
	public function getBookingPrimaryElectronicAddress();

	/**
	 * @return ElectronicAddress
	 */
	public function getBookingSecondaryElectronicAddress();

	/**
	 * @return string
	 */
	public function getTransactionId();

	/**
	 * @return string
	 */
	public function getUniqueId();

	/**
	 * @return string
	 */
	public function getShortId();

	/**
	 * @return string
	 */
	public function getUsage();

	/**
	 * @return string
	 */
	public function getLanguage();

}
