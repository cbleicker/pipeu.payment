<?php
namespace PIPEU\Payment\Http\Client;

/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Http\Client\RequestEngineInterface;
use TYPO3\Flow\Http\Request;
use TYPO3\Flow\Http\Response;
use TYPO3\Flow\Http\Client\CurlEngineException;
use TYPO3\Flow\Http\Exception as HttpException;
use TYPO3\Flow\Reflection\ObjectAccess;

/**
 * Class CurlEngine
 *
 * @package PIPEU\Payment\Http\Client
 */
class CurlEngine implements RequestEngineInterface {

	/**
	 * @var array
	 */
	protected $options = array(
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HEADER => TRUE,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_FRESH_CONNECT => TRUE,
		CURLOPT_FORBID_REUSE => TRUE,
		CURLOPT_TIMEOUT => 30
	);

	/**
	 * Sets an option to be used by cURL.
	 *
	 * @param integer $optionName One of the CURLOPT_* constants
	 * @param mixed $value The value to set
	 */
	public function setOption($optionName, $value) {
		$this->options[$optionName] = $value;
	}

	/**
	 * Sends the given HTTP request
	 *
	 * @param \TYPO3\Flow\Http\Request $request
	 * @return \TYPO3\Flow\Http\Response The response or FALSE
	 * @api
	 * @throws \TYPO3\Flow\Http\Exception
	 * @throws CurlEngineException
	 */
	public function sendRequest(Request $request) {

		if (!extension_loaded('curl')) {
			throw new HttpException('CurlEngine requires the PHP CURL extension to be installed and loaded.', 1414427020);
		}

		$curlHandle = curl_init();
		curl_setopt_array($curlHandle, $this->options);
		curl_setopt($curlHandle, CURLOPT_URL, $request->getUri());
		curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, $request->getMethod());
		$content = ObjectAccess::getProperty($request, 'content', TRUE);
		$body = $content !== NULL ? $content : http_build_query($request->getArguments());
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $body);

		$preparedHeaders = array();
		foreach ($request->getHeaders()->getAll() as $fieldName => $values) {
			foreach ($values as $value) {
				$preparedHeaders[] = $fieldName . ': ' . $value;
			}
		}
		curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $preparedHeaders);

		if ($request->getUri()->getPort() !== NULL) {
			curl_setopt($curlHandle, CURLOPT_PORT, $request->getUri()->getPort());
		}

		$rawResponse = curl_exec($curlHandle);
		if($rawResponse === FALSE) {
			throw new CurlEngineException('cURL reported error code ' . curl_errno($curlHandle) . ' with message "' . curl_error($curlHandle) . '". Last requested URL was "' . curl_getinfo($curlHandle, CURLINFO_EFFECTIVE_URL) . '".', 1338906040);
		} elseif (strlen($rawResponse) === 0) {
			return FALSE;
		}
		curl_close($curlHandle);

		$response = Response::createFromRaw($rawResponse);
		if ($response->getStatusCode() === 100) {
			$response = Response::createFromRaw($response->getContent(), $response);
		}
		return $response;
	}
}
