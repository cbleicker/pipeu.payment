<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Http\Abstracts;

use PIPEU\Payment\Http\Interfaces\ApiResponseInterface;
use TYPO3\Flow\Http\Response;

/**
 * Class AbstractApiResponse
 *
 * @package PIPEU\Payment\Http\Abstracts
 */
abstract class AbstractApiResponse extends Response implements ApiResponseInterface {

}
