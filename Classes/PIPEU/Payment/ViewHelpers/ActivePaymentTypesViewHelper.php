<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\ViewHelpers;

use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use PIPEU\Payment\Domain\Service\Interfaces\PaymentTypeServiceInterface;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class ActivePaymentTypesViewHelper
 *
 * @package PIPEU\Payment\ViewHelpers
 */
class ActivePaymentTypesViewHelper extends AbstractViewHelper {

	/**
	 * @var PaymentTypeServiceInterface
	 * @Flow\Inject
	 */
	protected $paymentTypeService;

	/**
	 * @param string $as
	 * @return string
	 */
	public function render($as = 'activePaymentTypes') {
		$activePaymentTypes = $this->paymentTypeService->getActivePaymentTypes();
		$this->templateVariableContainer->add($as, $activePaymentTypes);
		$output = $this->renderChildren();
		$this->templateVariableContainer->remove($as);
		return $output;
	}
}
