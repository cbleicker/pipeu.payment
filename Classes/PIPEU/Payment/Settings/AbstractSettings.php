<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Payment\Settings;
use PIPEU\Payment\Settings\Interfaces\SettingsInterface;
use TYPO3\Flow\Reflection\ObjectAccess;

/**
 * Class AbstractSettings
 *
 * @package PIPEU\Payment\Settings
 */
abstract class AbstractSettings extends \ArrayObject implements SettingsInterface{

	/**
	 * @param string $path
	 * @return mixed
	 */
	public function getValueByPath($path) {
		return ObjectAccess::getPropertyPath($this, $path);
	}

	/**
	 * @param mixed $index
	 * @return $this
	 */
	public function offsetExists($index) {
		return parent::offsetExists($index);
	}

	/**
	 * @param mixed $index
	 * @return mixed
	 */
	public function offsetGet($index) {
		return parent::offsetGet($index);
	}

	/**
	 * @param mixed $index
	 * @param mixed $newval
	 * @return $this
	 */
	public function offsetSet($index, $newval) {
		parent::offsetSet($index, $newval);
		return $this;
	}

	/**
	 * @param mixed $index
	 * @return $this
	 */
	public function offsetUnset($index) {
		parent::offsetUnset($index);
		return $this;
	}

	/**
	 * @param mixed $value
	 * @return $this
	 */
	public function append($value) {
		parent::append($value);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getArrayCopy() {
		return parent::getArrayCopy();
	}

	/**
	 * @return integer
	 */
	public function count() {
		return parent::count();
	}

	/**
	 * @return integer
	 */
	public function getFlags() {
		return parent::getFlags();
	}

	/**
	 * @param integer $flags
	 * @return $this
	 */
	public function setFlags($flags) {
		parent::setFlags($flags);
		return $this;
	}

	/**
	 * @return $this
	 */
	public function asort() {
		parent::asort();
		return $this;
	}

	/**
	 * @return $this
	 */
	public function ksort() {
		parent::ksort();
		return $this;
	}

	/**
	 * @param callable $cmp_function
	 * @return $this
	 */
	public function uasort($cmp_function) {
		parent::uasort($cmp_function);
		return $this;
	}

	/**
	 * @param callable $cmp_function
	 * @return $this
	 */
	public function uksort($cmp_function) {
		parent::uksort($cmp_function);
		return $this;
	}

	/**
	 * @return $this
	 */
	public function natsort() {
		parent::natsort();
		return $this;
	}

	/**
	 * @return $this
	 */
	public function natcasesort() {
		parent::natcasesort();
		return $this;
	}

	/**
	 * @param string $serialized
	 * @return $this
	 */
	public function unserialize($serialized) {
		return parent::unserialize($serialized);
	}

	/**
	 * @return string
	 */
	public function serialize() {
		return parent::serialize();
	}

	/**
	 * @return \ArrayIterator
	 */
	public function getIterator() {
		return parent::getIterator();
	}

	/**
	 * @param mixed $input
	 * @return array
	 */
	public function exchangeArray($input) {
		return parent::exchangeArray($input);
	}

	/**
	 * @param string $iterator_class
	 * @return $this
	 */
	public function setIteratorClass($iterator_class) {
		parent::setIteratorClass($iterator_class);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getIteratorClass() {
		parent::getIteratorClass();
	}

}
